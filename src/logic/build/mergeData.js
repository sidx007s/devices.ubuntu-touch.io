import slugify from "@sindresorhus/slugify";

import releasesData from "@data/releases.json";
import * as dataUtils from "./dataUtils.js";

export function mergeReleases(devices, releases) {
  let mergedDevices = [];

  devices.forEach((device) => {
    let deviceReleases = releases.filter((r) => r.codename == device.codename);
    device.allReleases = deviceReleases.map((r) => {
      let isDefault = r.release == releasesData.default;
      return {
        name: r.release,
        path:
          "/device/" +
          slugify(device.codename, { decamelize: false }) +
          (!isDefault ? "/release/" + r.release : ""),
        default: isDefault
      };
    });

    if (deviceReleases.length == 0) {
      mergedDevices.push(device);
      return;
    }

    deviceReleases.forEach((release) => {
      // Copy device and merge data for each release
      let mergedDevice = JSON.parse(JSON.stringify(device));
      let currentRelease = releasesData.list.find(
        (r) => r.name == release.release
      );
      Object.assign(mergedDevice, release);

      // Add release props to the schema
      mergedDevice.warnings = mergedDevice.warnings || {};
      if (currentRelease.dev) mergedDevice.warnings.dev = true;
      if (currentRelease.obsolete) mergedDevice.warnings.obsolete = true;
      if (currentRelease.hidden) mergedDevice.unlisted = true;
      mergedDevices.push(mergedDevice);
    });
  });
  return mergedDevices;
}

export function mergeVariants(devices) {
  return devices.map((device) => {
    if (!device.variantOf) return device;
    if (!device.name) device.unlisted = true;

    let deviceClone = JSON.parse(
      JSON.stringify(devices.find((d) => d.codename == device.variantOf))
    );
    for (let key in device) {
      if (key == "filePath") continue;
      if (key == "content" && !!device[key]) continue;
      if (!["array", "object"].includes(typeof device[key])) {
        deviceClone[key] = device[key];
      } else {
        if (
          [
            "contributors",
            "communityHelp",
            "docLinks",
            "externalLinks"
          ].includes(key)
        )
          deviceClone[key].concat(device[key]);
        else if (key == "portStatus") {
          if (!deviceClone[key]) deviceClone[key] = device[key];
          else
            device[key].forEach((category) => {
              let cloneCategory = deviceClone[key].findIndex(
                (c) => c.categoryName == category.categoryName
              );
              if (cloneCategory == -1) deviceClone[key].push(category);
              else
                category.features.forEach((feature) => {
                  let cloneFeature = deviceClone[key][
                    cloneCategory
                  ].features.findIndex((f) => f.id == feature.id);
                  if (cloneFeature == -1)
                    deviceClone[key][cloneCategory].features.push(feature);
                  else
                    deviceClone[key][cloneCategory].features[cloneFeature] =
                      feature;
                });
            });
        } else if (key == "deviceInfo") {
          if (!deviceClone[key]) deviceClone[key] = device[key];
          device[key].forEach((specification) => {
            let cloneSpecification = deviceClone[key].findIndex(
              (s) => s.id == specification.id
            );
            if (cloneSpecification == -1) deviceClone[key].push(specification);
            else deviceClone[key][cloneSpecification] = specification;
          });
        } else if (["price", "seo"].includes(key)) {
          Object.assign(deviceClone[key], device[key]);
        }
      }
    }
    return deviceClone;
  });
}
