export function startTracking(url, siteId) {
  window._paq = window._paq || [];
  _paq.push(["trackPageView"]);
  _paq.push(["enableLinkTracking"]);
  (function () {
    _paq.push(["setTrackerUrl", url + "piwik.php"]);
    _paq.push(["setSiteId", siteId]);
    var d = document,
      g = d.createElement("script"),
      s = d.getElementsByTagName("script")[0];
    g.async = true;
    g.src = url + "piwik.js";
    s.parentNode.insertBefore(g, s);
  })();
}

export function spaNavigationTrack() {
  _paq.push(["setCustomUrl", window.location.href]);
  _paq.push(["setDocumentTitle", document.title]);
  _paq.push(["trackPageView"]);
  _paq.push(["enableLinkTracking"]);
}
