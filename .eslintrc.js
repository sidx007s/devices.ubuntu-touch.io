module.exports = {
  parserOptions: {
    ecmaVersion: "latest",
    sourceType: "module"
  },
  extends: ["plugin:md/recommended", "plugin:prettier/recommended", "prettier"],
  rules: {
    "prettier/prettier": "error",
    "md/remark": "off"
  },
  overrides: [
    {
      files: ["*.md"],
      parser: "markdown-eslint-parser",
      rules: {
        "prettier/prettier": ["error", { parser: "markdown" }]
      }
    },
    {
      files: ["*.astro"],
      parser: "astro-eslint-parser"
    }
  ]
};
